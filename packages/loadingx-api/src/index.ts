type ActiveLoadingStateType = "preparing" | "waiting" | "received" | "finished";
export type LoadingStateType = boolean | ActiveLoadingStateType;

export interface ILoadingState {
  set(state?: LoadingStateType): void;
  unset(): void;
  readonly state: LoadingStateType;
  readonly isLoading: boolean;
}
