import { useInstance } from "@ki2/utils-react";

import { LoadingState } from ".";

export function useLoadingState() {
  return useInstance(() => new LoadingState());
}
