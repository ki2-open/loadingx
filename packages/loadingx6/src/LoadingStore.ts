import { makeAutoObservable } from "mobx";
import { exist, isBoolean } from "@ki2/utils";
import type { LoadingStateType, ILoadingState } from "@ki2/loadingx-api";

export class LoadingState implements ILoadingState {
  protected _state: LoadingStateType = false;

  constructor() {
    makeAutoObservable(this);
  }

  set(state?: LoadingStateType): void {
    if (!exist(state)) {
      this._state = true;
    } else {
      this._state = state;
    }
  }

  unset(): void {
    this._state = false;
  }

  get state(): LoadingStateType {
    return this._state;
  }

  get isLoading(): boolean {
    if (isBoolean(this._state)) {
      return this._state;
    }
    return this._state !== "finished";
  }
}
