import { action, computed, observable } from "mobx";
import { exist, isBoolean } from "@ki2/utils";
import type { LoadingStateType, ILoadingState } from "@ki2/loadingx-api";

export class LoadingState implements ILoadingState {
  @observable private _state: LoadingStateType = false;

  @action set(state?: LoadingStateType): void {
    if (!exist(state)) {
      this._state = true;
    } else {
      this._state = state;
    }
  }

  @action unset(): void {
    this._state = false;
  }

  @computed get state(): LoadingStateType {
    return this._state;
  }

  @computed get isLoading(): boolean {
    if (isBoolean(this._state)) {
      return this._state;
    }
    return this._state !== "finished";
  }
}
